<?php

define('ROOT_PATH', str_replace('\\', '/', __DIR__) . '/');
define('TMP_PATH', ROOT_PATH . '/tmp/');

/**
 * 获取配置 db.user.test
 */
function C($path) {

	$config_dir = ROOT_PATH . '/config/';
	$config = explode('.', trim($path, '.'));
	$len = count($config);

	if ($len === 1) {
		if (file_exists($config_dir . $config[0] . '.php')) {
			return require $config_dir . $config[0] . '.php';
		}
		return array();
	}

	$c = require $config_dir . array_shift($config) . '.php';

	$eval_str = '$c';
	foreach ($config as $k) {
		$eval_str .= "['{$k}']";
	}
	@eval('$c = ' . $eval_str . ';');

	return $c;
}