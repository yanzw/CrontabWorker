<?php
namespace CrontabWorker;


class MsgQueue {

	public $key = null;
	public $length = 1024; //队列长度

	static public $queue = null;


	public function __construct($key, $mode=0666)
	{
		$this->key = $key;
		self::$queue = \msg_get_queue($key,$mode);
		if (!self::$queue) {
			throw new \Exception("$key faild!", 1);
		}
	}

	/**
	 * 创建队列
	 * @param  [type]  $key  [description]
	 * @param  integer $mode [description]
	 * @return [type]        [description]
	 */
	static public function create($key,$mode=0666) 
	{
		return new self($key,$mode);
	}

	/**
	 * 入列
	 * @param  [type] $msg 消息
	 * @param  [type] $unserialize 数据是否序列化
	 */
	public function push($msg, $unserialize = false)
	{
		$stat = \msg_stat_queue(self::$queue);
		if ($stat['msg_qnum'] >= $this->length ) {
			throw new \Exception("error: msg_queue: {$this->key} full!", 1);
		}
		return \msg_send(self::$queue, 1, $msg, $unserialize);
	}

	/**
	 * 出队,默认阻塞
	 */
	public function pop()
	{
		$message_type = 0;
		\msg_receive(self::$queue, 0, $message_type, 1024, $message, false);
		if (!$message) {
			return false;
		}
		return $message;
	}
	
	public function clear()
	{
		\msg_remove_queue(self::$queue);
	}
	
}
