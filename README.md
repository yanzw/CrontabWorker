# CrontabWorker

## 纯PHP多进程定时任务调度服务器框架，支持守护进程，采用预先派生进程模型，主进程担任定时任务的分发生产者，子进程负责消费队列任务，任务调度触发使用消息队列

## 环境要求

1. Liunx
2. pcntl扩展开启
3. php 5.3以上
4. composer

### 使用自带示例

1.开始

> php tests/test1.php start

2.停止

> php tests/test1.php stop

3.查看任务运行状态

> php tests/test1.php status

```
======================================================================
PPID: 27431
RUN_TIME: 2017-11-15 22:41
PROCCESS_TITLE: php-crontab
PROCCESS: 5
======================================================================
NAME           COMMAND   COUNT     LAST_TIME           NEXT_TIME
任务1          at@11:00  0         none                11-16 11:00:00
任务2          s@1       1123      11-15 23:00:57      11-15 23:00:58
任务3          s@60      19        11-15 23:00:34      11-15 23:01:35
任务4          i@1       19        11-15 23:00:34      11-15 23:01:35
任务5          h@1       0         none                11-15 23:41:33
```

3. 代码示例
```
<?php

require __DIR__ . '/../vendor/autoload.php';

ini_set("display_errors", "On");

use \CrontabWorker\CrontabWorker;


$test = new CrontabWorker();

$test->proccessNum = 5;//预先派生进程数
$test->daemon = true; //开启守护进程
$test->output = './tmp/test.log'; //重定向服务输出


$test->addInterval('任务1', 'at@11:00', "\\Scripts\\Test::test",array("测试每11:00秒记录信息"));

$test->addInterval('任务2', 's@1', "\\Scripts\\Test::test",array("测试每1秒记录信息"));
$test->addInterval('任务3', 's@60', "\\Scripts\\Test::test",array("测试每60秒记录信息"));

$test->addInterval('任务4', 'i@1', "\\Scripts\\Test::test",array("测试每1分钟记录信息"));
$test->addInterval('任务5', 'h@1', "\\Scripts\\Test::test",array("测试每1小时记录信息"));

$test->run();

```