<?php

require __DIR__ . '/../vendor/autoload.php';

ini_set("display_errors", "On");

use \CrontabWorker\CrontabWorker;


$test = new CrontabWorker();

$test->proccessNum = 5;//预先派生进程数
$test->daemon = true; //开启守护进程
$test->output = './tmp/test.log'; //重定向服务输出


$test->addInterval('任务1', 'at@11:00', "\\Scripts\\Test::test",array("测试每11:00秒记录信息"));

$test->addInterval('任务2', 's@1', "\\Scripts\\Test::test",array("测试每1秒记录信息"));
$test->addInterval('任务3', 's@60', "\\Scripts\\Test::test",array("测试每60秒记录信息"));

$test->addInterval('任务4', 'i@1', "\\Scripts\\Test::test",array("测试每1分钟记录信息"));
$test->addInterval('任务5', 'h@1', "\\Scripts\\Test::test",array("测试每1小时记录信息"));

$test->run();