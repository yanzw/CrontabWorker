<?php
namespace Scripts;

class Test{
	
	public static function test($text)
	{
		$file = ROOT_PATH . '/tmp/' . $text . '.log';
		if (!is_file($file)) {
			touch($file);
			chmod($file, 0666);
		}
		
		file_put_contents($file, $text . " [".date('Y-m-d H:i:s')."]" . "\n", FILE_APPEND);
	}

}